# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/17 14:22:50 by tlovato           #+#    #+#              #
#    Updated: 2016/07/25 14:31:13 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 		fillit

W_FLAGS =	-Wall -Wextra -Werror

SRC_DIR =	src/
INC_DIR =	inc/

FILES =		main.c file_parse.c verif_format.c add_to_piece_lst.c \
			verif_piece_list.c init_resolve.c \
			square_size.c maths.c \
			create_bmap.c create_data.c \
			link_data_reality.c resolve.c can_be_there.c \
			print_soluce.c history.c

SRC = 		$(addprefix $(SRC_DIR), $(FILES))
OBJ = 		$(SRC:.c=.o)

LIB = 		-L libft/ -lft
INC =		-I /usr/local/include/ -I $(INC_DIR) -I libft/

LIBFT =		libft/libft.a

all:		$(NAME)

$(NAME):	$(OBJ) $(LIBFT)
			gcc -o $@ $^ $(INC) $(LIB) $(W_FLAGS)

$(LIBFT):
			make -C libft/

%.o:		%.c
			gcc -o $@ -c $^ $(INC)

clean:
			rm -f $(OBJ)

fclean: 	clean
			rm -f $(NAME)

re: 		fclean all

.PHONY:		all clean fclean re
