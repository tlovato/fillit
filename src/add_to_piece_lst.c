/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_to_piece_lst.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 13:20:03 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/19 14:24:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char		*make_piece_map(char *str)
{
	char		*map;
	int			i;
	int			j;

	map = (char *)malloc(sizeof(char) * 17);
	i = 0;
	j = 0;
	while (i < 20)
	{
		while (str[i] != '\n')
		{
			if (str[i] == '#')
				map[j] = 1;
			else
				map[j] = 0;
			i++;
			j++;
		}
		i++;
	}
	map[j] = 0;
	return (map);
}

void			add_to_piece_lst(t_piece **list, char *buf)
{
	t_piece		*dlist;
	t_piece		*nlist;

	dlist = *list;
	nlist = (t_piece *)malloc(sizeof(t_piece));
	nlist->next = NULL;
	nlist->map = make_piece_map(buf);
	if (!(*list))
	{
		*list = nlist;
		return ;
	}
	while (dlist->next != NULL)
		dlist = dlist->next;
	dlist->next = nlist;
}
