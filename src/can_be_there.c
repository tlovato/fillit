/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   can_be_there.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:33:20 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:33:27 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				can_be_there(t_env *env, t_piece *piece, int *pos)
{
	int			k;

	k = 0;
	while (k < env->data->dim_max[piece->id][1])
	{
		if (((piece->bmap[k] >> pos[0]) & env->bbox[pos[1] + k]) != 0)
			return (0);
		k++;
	}
	return (1);
}
