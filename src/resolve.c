/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   resolve.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:34:48 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:34:49 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		placement(t_env *env, t_piece *piece, int *pos)
{
	int			k;

	k = 0;
	while (k < env->data->dim_max[piece->id][1])
	{
		env->bbox[pos[1] + k] ^= (piece->bmap[k] >> pos[0]);
		k++;
	}
	add_to_pos_history(env, piece->id, pos);
}

static int		deplacement(t_env *env, t_piece *piece, int *pos)
{
	int			k;

	k = 0;
	while (k < env->data->dim_max[piece->id][1])
	{
		env->bbox[pos[1] + k] ^= (piece->bmap[k] >> pos[0]);
		k++;
	}
	env->data->index_history[piece->id] -= 1;
	return (1);
}

static int		set_final_pos(t_env *env, t_piece *pce, int *pos)
{
	pce->pos_bbox[0] = pos[0];
	pce->pos_bbox[1] = pos[1];
	return (deplacement(env, pce, pos));
}

int				backtrack(t_env *e, t_piece *pce, int s_max)
{
	int			pos[2];

	if (pce == NULL)
		return (1);
	pos[1] = e->data->pos_history[pce->id][e->data->index_history[pce->id]][1];
	pos[0] = e->data->pos_history[pce->id][e->data->index_history[pce->id]][0];
	while (pos[1] < e->data->shift[e->data->dim_max[pce->id][1] - 1][s_max])
	{
		if (can_be_there(e, pce, pos))
		{
			placement(e, pce, pos);
			if (backtrack(e, pce->next, s_max))
				return (set_final_pos(e, pce, pos));
			deplacement(e, pce, pos);
		}
		pos[0]++;
		if (pos[0] >= e->data->shift[e->data->dim_max[pce->id][0] - 1][s_max])
		{
			pos[1]++;
			pos[0] = 0;
		}
	}
	return (0);
}

int				resolve(t_env *env)
{
	int			size_max;

	size_max = ft_sqrt(env->n_piece * 4);
	env->piece->pos_bbox[0] = -1;
	while (size_max < env->n_piece * 4)
	{
		backtrack(env, env->piece, size_max);
		if (env->piece->pos_bbox[0] != -1)
			return (size_max);
		size_max++;
	}
	return (size_max);
}
