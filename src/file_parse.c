/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_parse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 14:18:29 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/21 21:50:39 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		error(int fd)
{
	close(fd);
	return (1);
}

int				file_parse(t_env *env, char *file_name)
{
	int			fd;
	int			ret[2];
	char		buf[22];

	env->piece = NULL;
	if ((fd = open(file_name, O_RDONLY)) == -1)
		return (1);
	ft_bzero(buf, 21);
	while ((ret[0] = read(fd, buf, 21)) != 0)
	{
		if (ret[0] < 20)
			return (error(fd));
		if (verif_format(buf, ret[0]))
			return (error(fd));
		add_to_piece_lst(&(env->piece), buf);
		ft_bzero(buf, 21);
		ret[1] = ret[0];
	}
	if (ret[1] != 20)
		return (error(fd));
	if (verif_piece_list(env))
		return (error(fd));
	close(fd);
	return (0);
}
