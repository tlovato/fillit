/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_bmap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:33:40 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:33:43 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		shift_line(t_piece *piece)
{
	while (piece->bmap[0] == 0)
	{
		piece->bmap[0] = piece->bmap[1];
		piece->bmap[1] = piece->bmap[2];
		piece->bmap[2] = piece->bmap[3];
		piece->bmap[3] = 0;
	}
}

void			shift_column(uint16_t *bmap)
{
	int			k;
	int			i;

	k = 0;
	i = 0;
	while (k == 0)
	{
		bmap[0] <<= 1;
		bmap[1] <<= 1;
		bmap[2] <<= 1;
		bmap[3] <<= 1;
		i = 0;
		while (i < 4)
		{
			if (bmap[i] & 0x8000)
				k = 1;
			i++;
		}
	}
}

static void		shift_them(t_piece **pce)
{
	t_piece		*tmp;

	tmp = *pce;
	shift_line(tmp);
	shift_column(tmp->bmap);
	*pce = (*pce)->next;
}

void			create_bmap(t_piece *piece, int i, int j)
{
	t_piece		*tmp;

	tmp = piece;
	while (tmp)
	{
		i = 0;
		tmp->bmap = (uint16_t *)malloc(sizeof(uint16_t) * 4);
		while (i < 4)
			tmp->bmap[i++] = 0;
		j = 0;
		i = 0;
		while (j++ < 16)
		{
			if ((j - 1) % 4 == 0 && (j - 1) != 0)
				i++;
			if (tmp->map[j - 1] != 0)
			{
				tmp->bmap[i] <<= 1;
				tmp->bmap[i] ^= 1;
			}
			else
				tmp->bmap[i] <<= 1;
		}
		shift_them(&tmp);
	}
}
