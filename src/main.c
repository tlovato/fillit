/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 13:28:33 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/21 23:27:15 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_error(void)
{
	ft_putstr("error\n");
	return (1);
}

static int		ft_usage(void)
{
	ft_putstr("usage: ./fillit source_file\n");
	return (1);
}

int				main(int ac, char **av)
{
	t_env		*env;

	if ((env = (t_env *)malloc(sizeof(t_env))) == NULL)
		return (1);
	if (ac != 2)
		return (ft_usage());
	if (file_parse(env, av[1]) == 1)
		return (ft_error());
	create_bmap(env->piece, 0, 0);
	create_data(env);
	if (link_data_reality(env) == -1)
		return (ft_error());
	init_resolve(env);
	print_soluce(env, resolve(env), 'A');
	return (0);
}
