/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   maths.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:34:28 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:34:29 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_puis(int nb, int p)
{
	int		n;

	if (p == 0)
		return (1);
	n = nb;
	while (p > 1)
	{
		nb *= n;
		p--;
	}
	return (nb);
}

int			ft_sqrt(int nb)
{
	int		x;

	x = 1;
	while (x * x < nb)
		x++;
	return (x);
}

int			ft_valabs(int n)
{
	if (n < 0)
		return (-n);
	return (n);
}
