/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square_size.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:35:00 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:35:02 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			square_size(char **sandbox, int n_piece)
{
	int		best_size;
	int		x;
	int		y;

	best_size = 4 * n_piece;
	while (best_size > 4)
	{
		x = 0;
		y = best_size - 1;
		while (x < best_size)
		{
			if (sandbox[x][y] == 1)
				return (best_size);
			if (sandbox[y][x] == 1)
				return (best_size);
			x++;
		}
		best_size--;
	}
	return (best_size);
}
