/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 16:14:00 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/18 14:53:05 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				verif_format(char *str, int ret)
{
	int			i;

	i = 0;
	while (i < 20)
	{
		if (i % 5 == 4)
		{
			if (str[i] != '\n')
				return (1);
		}
		else
		{
			if (str[i] != '#' && str[i] != '.')
				return (1);
		}
		i++;
	}
	if (ret == 21 && str[i] != '\n')
		return (1);
	return (0);
}
