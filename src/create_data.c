/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_data.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:33:53 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:33:57 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static uint16_t		*char_to_bmap(char *str)
{
	uint16_t		*bmap;
	int				i;
	int				k;

	bmap = (uint16_t *)malloc(sizeof(uint16_t) * 4);
	bmap[0] = 0;
	bmap[1] = 0;
	bmap[2] = 0;
	bmap[3] = 0;
	i = 0;
	k = 0;
	while (str[i])
	{
		if (i % 4 == 0 && i != 0)
			k++;
		if (str[i] == '1')
		{
			bmap[k] <<= 1;
			bmap[k] ^= 1;
		}
		else
			bmap[k] <<= 1;
		i++;
	}
	return (bmap);
}

static void			data_add_tetriminos(t_env *env)
{
	env->data->bmap = (uint16_t **)malloc(sizeof(uint16_t *) * 19);
	env->data->bmap[0] = char_to_bmap("1000100011000000");
	env->data->bmap[1] = char_to_bmap("0010111000000000");
	env->data->bmap[2] = char_to_bmap("1100010001000000");
	env->data->bmap[3] = char_to_bmap("1110100000000000");
	env->data->bmap[4] = char_to_bmap("1000100010001000");
	env->data->bmap[5] = char_to_bmap("1111000000000000");
	env->data->bmap[6] = char_to_bmap("0100010011000000");
	env->data->bmap[7] = char_to_bmap("1110001000000000");
	env->data->bmap[8] = char_to_bmap("1100100010000000");
	env->data->bmap[9] = char_to_bmap("1000111000000000");
	env->data->bmap[10] = char_to_bmap("1100110000000000");
	env->data->bmap[11] = char_to_bmap("1000110001000000");
	env->data->bmap[12] = char_to_bmap("0110110000000000");
	env->data->bmap[13] = char_to_bmap("0100110010000000");
	env->data->bmap[14] = char_to_bmap("1100011000000000");
	env->data->bmap[15] = char_to_bmap("1110010000000000");
	env->data->bmap[16] = char_to_bmap("0100110001000000");
	env->data->bmap[17] = char_to_bmap("0100111000000000");
	env->data->bmap[18] = char_to_bmap("1000110010000000");
}

static void			data_add_dim(t_env *env, int i)
{
	int				j;
	int				k;
	int				l;

	while (i < 19)
	{
		j = 3;
		while (env->data->bmap[i][j] == 0 && j > 0)
			j--;
		env->data->dim_max[i][1] = j + 1;
		j = 4;
		k = 4;
		l = 0;
		while (k == 4 && j > 0)
		{
			k = 0;
			while ((env->data->bmap[i][k] & ft_puis(2, l)) == 0 && k < 4)
				k++;
			l++;
			j--;
		}
		env->data->dim_max[i][0] = j + 1;
		shift_column(env->data->bmap[i]);
		i++;
	}
}

static void			data_add_shift(t_env *env)
{
	int				dim;
	int				sq_size;

	dim = 0;
	while (dim < 4)
	{
		sq_size = 0;
		while (sq_size < 16)
		{
			env->data->shift[dim][sq_size] = sq_size - dim;
			sq_size++;
		}
		dim++;
	}
}

void				create_data(t_env *env)
{
	env->data = (t_data *)malloc(sizeof(t_data));
	data_add_tetriminos(env);
	data_add_dim(env, 0);
	data_add_shift(env);
}
