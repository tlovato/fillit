/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_resolve.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 20:08:12 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/21 23:30:33 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		init_sandbox(t_env *env)
{
	int		i;

	i = 0;
	env->sandbox = (char **)malloc(sizeof(char *) * (16));
	while (i < (16))
	{
		env->sandbox[i] = (char *)malloc(sizeof(char ) * (16));
		ft_bzero(env->sandbox[i], (16));
		i++;
	}
}

static void		init_bbox(t_env *env)
{
	int			i;

	i = 0;
	while (i < 16)
	{
		env->bbox[i] = 0;
		i++;
	}
}

void			init_resolve(t_env *env)
{
	init_sandbox(env);
	init_bbox(env);
}
