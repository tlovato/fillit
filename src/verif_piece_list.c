/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_piece_list.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 15:12:36 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/21 23:02:45 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		find_survivors(char *str)
{
	int		i;

	i = 0;
	while (i < 16)
	{
		if (str[i] == 1)
			return (1);
		i++;
	}
	return (0);
}

static char		*path_finder(char *str, int i)
{
	char		*path;

	path = (char *)malloc(sizeof(char) * 4);
	path[0] = -4;
	path[1] = 4;
	path[2] = -1;
	path[3] = 1;
	if (i >= 0 && i < 4)
		path[0] = 0;
	if (i % 4 == 3)
		path[3] = 0;
	if (i >= 12 && i < 16)
		path[1] = 0;
	if (i % 4 == 0)
		path[2] = 0;
	return (path);
}

static int		infection(char *str, int position)
{
	char		*path;
	int			victims;
	int			i;

	if (str[position] != 1)
		return (0);
	path = path_finder(str, position);
	str[position] = 2;
	victims = 1;
	i = 0;
	while (i < 4)
	{
		if (path[i] != 0)
			victims += infection(str, position + path[i]);
		i++;
	}
	return (victims);
}

static int		verif_piece(char *str)
{
	int			i;
	int			victims;

	i = 0;
	victims = 0;
	while (i < 16 && str[i] != 1)
		i++;
	if (i == 16)
		return (1);
	victims = infection(str, i);
	if (victims != 4)
		return (1);
	if (find_survivors(str))
		return (1);
	return (0);
}

int				verif_piece_list(t_env *env)
{
	t_piece		*tmp;

	tmp = env->piece;
	env->n_piece = 0;
	while (tmp)
	{
		if (verif_piece(tmp->map))
			return (1);
		tmp = tmp->next;
		env->n_piece++;
	}
	return (0);
}
