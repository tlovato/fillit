/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_data_reality.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:34:18 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:34:19 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	get_id_data(uint16_t *bmap, t_data *data)
{
	int		i;

	i = 0;
	while (i < 19)
	{
		if (data->bmap[i][0] == bmap[0])
			if (data->bmap[i][1] == bmap[1])
				if (data->bmap[i][2] == bmap[2])
					if (data->bmap[i][3] == bmap[3])
						return (i);
		i++;
	}
	return (-1);
}

int			link_data_reality(t_env *env)
{
	t_piece	*piece;

	piece = env->piece;
	while (piece)
	{
		piece->id = get_id_data(piece->bmap, env->data);
		if (piece->id == -1)
			return (-1);
		piece = piece->next;
	}
	return (0);
}
