/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_soluce.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:34:37 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:34:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		placement(t_env *env, t_piece *piece, int *pos, int id)
{
	int			i;

	i = 0;
	while (i < 4)
	{
		if ((piece->bmap[i] & 0x8000) == 0x8000)
			env->sandbox[pos[0]][pos[1] + i] = id;
		if ((piece->bmap[i] & 0x4000) == 0x4000)
			env->sandbox[pos[0] + 1][pos[1] + i] = id;
		if ((piece->bmap[i] & 0x2000) == 0x2000)
			env->sandbox[pos[0] + 2][pos[1] + i] = id;
		if ((piece->bmap[i] & 0x1000) == 0x1000)
			env->sandbox[pos[0] + 3][pos[1] + i] = id;
		i++;
	}
}

void			print_soluce(t_env *env, int sq_size, char id)
{
	t_piece		*tmp;
	int			x;
	int			y;

	tmp = env->piece;
	while (tmp)
	{
		placement(env, tmp, tmp->pos_bbox, (int)id++);
		tmp = tmp->next;
	}
	x = 0;
	while (x < sq_size)
	{
		y = 0;
		while (y < sq_size)
		{
			if (env->sandbox[y][x] >= 'A' && env->sandbox[y][x] <= 'Z')
				ft_putchar((char)env->sandbox[y][x]);
			else
				ft_putchar('.');
			y++;
		}
		ft_putchar('\n');
		x++;
	}
}
