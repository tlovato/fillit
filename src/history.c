/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 18:34:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/02/01 18:34:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			add_to_pos_history(t_env *env, int id, int *pos)
{
	env->data->index_history[id]++;
	env->data->pos_history[id][env->data->index_history[id]][0] = pos[0];
	env->data->pos_history[id][env->data->index_history[id]][1] = pos[1];
}

void			init_pos_history(t_env *env)
{
	int			i;
	int			j;
	int			k;

	i = 0;
	while (i < 19)
	{
		env->data->index_history[i] = 0;
		j = 0;
		while (j < 27)
		{
			env->data->pos_history[i][j][0] = 0;
			env->data->pos_history[i][j][1] = 0;
			j++;
		}
		i++;
	}
}
