/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 13:40:46 by tlovato           #+#    #+#             */
/*   Updated: 2016/06/21 12:13:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>
# include "libft.h"

/*
**	- STRUCTURES DE DONNES	____
*/
typedef struct			s_pos
{
	int					x;
	int					y;
}						t_pos;

typedef struct			s_piece
{
	char				*map;
	uint16_t			*bmap;
	int					id;
	int					pos_bbox[2];
	int					pos_anchor[2];
	int					pos_sandbox[2];
	int					x[4];
	int					y[4];
	struct s_piece		*next;
}						t_piece;

typedef struct			s_bestsquare
{
	int					size;
	t_piece				*piece;
}						t_bestsquare;

typedef struct			s_data
{
	uint16_t			**bmap;
	int					dim_max[19][2];
	int					shift[4][16];
	int					pos_history[19][27][2];
	int					index_history[19];
}						t_data;

typedef struct			s_env
{
	uint16_t			bbox[16];
	char				**sandbox;
	t_data				*data;
	t_piece				*piece;
	int					n_piece;
	t_bestsquare		*best_square;
}						t_env;

/*
**	- FONCTIONS				_____
*/
int						can_be_there(t_env *env, t_piece *piece, int *pos);
int						file_parse(t_env *env, char *file_name);
int						ft_puis(int nb, int p);
int						ft_sqrt(int nb);
int						ft_valabs(int nb);
int						link_data_reality(t_env *env);
int						resolve(t_env *env);
int						square_size(char **sandbox, int n_piece);
int						verif_format(char *str, int ret);
int						verif_piece_list(t_env *env);
void					add_to_piece_lst(t_piece **list, char *buf);
void					add_to_pos_history(t_env *env, int id, int *pos);
void					create_bmap(t_piece *piece, int i, int j);
void					create_data(t_env *env);
void					init_pos_history(t_env *env);
void					init_resolve(t_env *env);
void					print_soluce(t_env *env, int size_max, char id);
void					rm_from_pos_history(t_env *env, int id);
void					shift_column(uint16_t *bmap);

#endif
